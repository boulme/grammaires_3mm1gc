# CTD de période 2 (introduction au traitement informatique des langages informatiques)

Les cours sont organisés en "chapitres". 

- Chapitre 1: introduction aux concepts de base. [diapos 1up](chap1.pdf) ou [diapos 4up](chap1-4up.pdf) 
- Chapitre 2: définition des langages algébriques (et BNF). [diapos 1up](chap2.pdf) ou [diapos 4up](chap2-4up.pdf) 
- Chapitre 3: théorie et applications des syntaxes abstraites (et des syntaxes préfixes). [diapos 1up](chap3.pdf) ou [diapos 4up](chap3-4up.pdf) 
- Chapitre 4: Sémantiques des BNF attribuées et arbres d'analyse [diapos 1up](chap4.pdf) ou [diapos 4up](chap4-4up.pdf) 
- Chapitre 5: théorie des grammaires et de l'analyse syntaxique LL(1) [diapos 1up](chap5.pdf) ou [diapos 4up](chap5-4up.pdf), avec [feuille d'exos sur l'analyse syntaxique LL(1)](tds-parsing.pdf) et sa [correction](tds-parsing-correction.pdf).

## Une technique efficace pour apprendre le cours

Il est important de savoir refaire les exos marqués d'un †.
La meilleure façon de travailler les cours est donc de refaire ces exos après chaque cours, le lendemain ou quelques jours plus tard, mais si possible avant le cours suivant. "Savoir refaire", signifie **sans regarder la correction** qui ne doit vous servir qu'à vérifier que vous avez réussi. Si vous ne réussissez pas à faire un exo **sans l'aide de la correction**, il faudra retravailler cette correction pour bien la comprendre (si vous ne la comprenez pas, demandez de l'aide à vos camarades ou à votre enseignant) et ressayer de refaire cet exo le lendemain ou les jours suivants. 

Cette technique de travail repose sur l'idée que vous devez intégrer un certain nombre d'*images mentales* et de *gestes techniques*, qu'il faut **mémoriser**. La **répétition** de manière **active**, c'est-à-dire en autonomie, est essentielle à cette mémorisation. Voir [la courbe de l'oubli](https://en.wikipedia.org/wiki/Forgetting_curve#Increasing_rate_of_learning) et [les révisions actives](https://en.wikipedia.org/wiki/Testing_effect).

Rem: avec une pratique systématique, il ne me semble pas nécessaire de refaire en autonomie chaque exercice plus d'une fois. En effet, les exercices ultérieurs du cours remobilisent de toute façon les connaissances antérieures...
