#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "prop.h"
#include "nnf.h"

// 
// definition of type Prop

struct Prop_Node {
        Prop_Root root;
        union {
                int id;   // case N_var
                Prop son; // case N_neg
                struct { Prop sonL, sonR; } bin; // case N_and, N_or, N_implies
        } u;
};

//
// definition of constant constructors
static struct Prop_Node _ptrue = {N_true};
Prop const ptrue = &_ptrue;

static struct Prop_Node _pfalse = {N_false};
Prop const pfalse = &_pfalse;

//
// definition of non-constant constructors
static Prop newProp(struct Prop_Node x) {
        struct Prop_Node *res = malloc(sizeof(struct Prop_Node));
        if (res==NULL) {
                printf("ERROR: unexpected null pointer. No memory left ?\n");
                exit(1);
        }
        *res = x;
        return res;
}

Prop var(int x) {
        assert (x > 0);
        return newProp((struct Prop_Node){N_var,{.id=x}});
}

Prop neg(Prop son) {
        assert (son != NULL);
        return newProp((struct Prop_Node){N_neg,{.son=son}});
}

Prop and(Prop sonL, Prop sonR) {
        assert (sonL != NULL);
        assert (sonR != NULL);
        return newProp((struct Prop_Node){N_and,{.bin={sonL,sonR}}});
}

Prop or(Prop sonL, Prop sonR) {
        assert (sonL != NULL);
        assert (sonR != NULL);
        return newProp((struct Prop_Node){N_or,{.bin={sonL,sonR}}});
}

Prop implies(Prop sonL, Prop sonR) {
        assert (sonL != NULL);
        assert (sonR != NULL);
        return newProp((struct Prop_Node){N_implies,{.bin={sonL,sonR}}});
}

/***********/
/* max_var */

static int max(int v1, int v2){
        return (v1>v2?v1:v2);
}

int max_var(Prop p){
        switch (p->root) {
        case N_true: case N_false:
                return 0;
        case N_var:
                return p->u.id;
        case N_neg:
                return max_var(p->u.son);
        case N_and: case N_or: case N_implies:
                return max(max_var(p->u.bin.sonL), max_var(p->u.bin.sonR));
        default:
                // This case should be impossible
                assert (1!=1);
                return 0;
        }
}

/********/
/* eval */

bool eval(Prop p, set env){
        switch (p->root) {
        case N_var:
                return in(p->u.id, env);
        /* A COMPLETER: AUTRES CAS */ 
        default:
                // This case should be impossible
                assert (1!=1);
                return false;
        }
}

/**********/
/* is_Nnf */

/* decide if p is a non-constant Nnf */ 
static bool is_Ncst(Prop p);

bool is_Nnf(Prop p){
        switch (p->root) {
        case N_true: case N_false:
                return true;
        default:
                return is_Ncst(p);
        }
}

static bool is_Ncst(Prop p){
        switch (p->root) {
        /* A COMPLETER: AUTRES CAS */ 
        default:
                return false;
        }
}



/**********/
/* to_Nnf */

static Nnf to_Nnfx(Prop p, bool has_neg);

Nnf to_Nnf(Prop p){
        return to_Nnfx(p, false);
}

static Nnf to_Nnfx(Prop p, bool b) {
        switch (p->root) {
        /* A COMPLETER: AUTRES CAS */ 
        default:
                // This case should be impossible
                assert (1!=1);
                return NULL;
        }
}


/*******************/
/* Prefix printing */

static void print_prefix_indent(int level, Prop p);

void print_prefix(Prop p) {
        print_prefix_indent(1,p);
}

static void print_prefix_indent(int level, Prop p){
        switch (p->root) {
        case N_true:
                printf("%*c\n",level,'t');
                break;
        case N_false:
                printf("%*c\n",level,'f');
                break;
        case N_var:
                printf("%*c%d\n",level-1,' ',p->u.id);
                break;
        case N_neg:
                printf("%*c\n",level,'-');
                print_prefix_indent(level+1,p->u.son);
                break;
        case N_and:
                printf("%*c\n",level,'&');
                print_prefix_indent(level+1,p->u.bin.sonL);
                print_prefix_indent(level+1,p->u.bin.sonR);
                break;
        case N_or:
                printf("%*c\n",level,'|');
                print_prefix_indent(level+1,p->u.bin.sonL);
                print_prefix_indent(level+1,p->u.bin.sonR);
                break;
        case N_implies:
                printf("%*c\n",level,'>');
                print_prefix_indent(level+1,p->u.bin.sonL);
                print_prefix_indent(level+1,p->u.bin.sonR);
                break;
        default:
                // This case should be impossible
                assert (1!=1);
        }
}


/******************/
/* Infix printing */

static void print_infix_precedence(int level, Prop p);

void print_infix(Prop p) {
        print_infix_precedence(3,p);
        printf("\n");
}

static void parenth(int father, int curr, char c) {
        if (father < curr) {
                printf("%c",c);
        }
}

static void print_infix_op(int father, int curr, int currL, char op, Prop sonL, Prop sonR) {
        parenth(father,curr,'(');
        print_infix_precedence(currL,sonL);
        printf("%c",op);
        print_infix_precedence(curr,sonR);
        parenth(father,curr,')');
}

static void print_infix_precedence(int level, Prop p) {
        switch (p->root) {
        case N_true:
                printf("t");
                break;
        case N_false:
                printf("f");
                break;
        case N_var:
                printf("%d",p->u.id);
                break;
        case N_neg:
                printf("-");
                print_infix_precedence(0,p->u.son);
                break;
        case N_and:
                print_infix_op(level,1,1,'&',p->u.bin.sonL,p->u.bin.sonR);
                break;
        case N_or:
                print_infix_op(level,2,2,'|',p->u.bin.sonL,p->u.bin.sonR);
                break;
        case N_implies:
                print_infix_op(level,3,2,'>',p->u.bin.sonL,p->u.bin.sonR);
                break;
        default:
                // This case should be impossible
                assert (1!=1);
        }
}

// MISC
Prop_Root root(Prop p){
        assert (p != NULL);
        return p->root;
}
