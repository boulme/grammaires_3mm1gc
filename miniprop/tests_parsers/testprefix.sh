#!/bin/bash

dir="$(dirname $0)"
cd ${dir} || exit 1
[ -d prefix/ ] || exit 1

# mode d'emploi
if [[ $# -eq 0 ]]; then 
    cat <<EOF
Usage: $0 args
  avec args = liste de noms de fichiers ".prop" du repertoire prefix/
  (le chemin contenant "prefix/" dans chaque nom est optionnel)

Exemples:
  $0 ok_mp.prop
  $0 ${dir}/prefix/*.prop
EOF
    exit 0
fi

make -C .. exe || exit 1


# autres cas 
res=prefix.$$

cat <<EOF
##
# Ci-dessous, pour chaque test:
#  - si vide: pas d'anomalie détectée
#  - sinon: diff sortie_attendu sortie_trouvée (sur format de sortie préfixe)
##
 
EOF


for fich in "$@" ; do
    name="$(basename ${fich})"
    echo "## TEST ${name}"
    if echo ${name} | egrep -q "^ok" ; then 
        ../prefix_prop -p < prefix/${name} > ${res}
        egrep -v "^#" prefix/${name} | diff - ${res}
    else
        ../prefix_prop -p < prefix/${name} > ${res}
        egrep "^#.*ERROR" prefix/${name} | sed -e 's/\# //g' | diff - ${res}
    fi
    rm -f ${res}
    echo
done
