#!/bin/bash

dir="$(dirname $0)"
cd ${dir} || exit 1
[ -d prefix/ ] || exit 1
[ -d infix/ ] || exit 1

# mode d'emploi
if [[ $# -eq 0 ]]; then 
    cat <<EOF
Usage: $0 args
  avec args = liste de noms de fichiers ".prop" du repertoire infix/
  (le chemin contenant "infix/" dans chaque nom est optionnel)

Exemples:
  $0 ok_mp.prop
  $0 ${dir}/infix/*.prop
EOF
    exit 0
fi

make -C .. exe || exit 1


# autres cas 
res=infix.$$

cat <<EOF
##
# Ci-dessous, pour chaque test:
#  - si vide: pas d'anomalie détectée
#  - sinon: diff sortie_attendu sortie_trouvée (sur format de sortie préfixe)
##
 
EOF


for fich in "$@" ; do
    name="$(basename ${fich})"
    echo "## TEST ${name}"
    if echo ${name} | egrep -q "^ok" ; then 
        ../infix_prop -p < infix/${name} > ${res}
        if [ -f prefix/${name} ]; then
            egrep -v "^#" prefix/${name} | diff - ${res}
        else
	    echo "# pas de sortie attendue ???"
	    cat ${res}
        fi
    else
        ../infix_prop -p < infix/${name} > ${res}
        egrep "^#.*ERROR" infix/${name} | sed -e 's/\# //g' | diff - ${res}
    fi
    rm -f ${res}
    echo
done
