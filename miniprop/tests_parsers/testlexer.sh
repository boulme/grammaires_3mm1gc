#!/bin/bash

cd "$(dirname $0)" || exit 1
[ -f prefix/ok_touslexemes.prop ] || exit 1
[ -f lexer_test.expected ] || exit 1

make -C .. exe || exit 1

# autres cas 
res=lexer.$$

cat <<EOF
##
# teste sur execution de ../debug_lexer < prefix/ok_touslexemes.prop
#  - si vide: pas d'anomalie détectée
#  - sinon: diff sortie_attendu sortie_trouvée
##
 
EOF

../debug_lexer < prefix/ok_touslexemes.prop > ${res}
diff lexer_test.expected ${res}
v=$?
rm -f ${res}
exit $v
