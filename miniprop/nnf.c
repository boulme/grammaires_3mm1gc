#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* We skip definition of Nnf */
#define _SORTS_SKIP_Nnf 1
#include "sorts.h"

/* Here we identify Nnf and Prop */
typedef Prop Nnf;

#include "nnf.h"
#include "prop.h"

//
// definition of constant constructors
Nnf nnf_true(){
        return ptrue;
}

Nnf nnf_false(){
        return pfalse;
}

// Smart constructors
Nnf nnf_literal(int x) {
        assert (x != 0);
        /* A CHANGER */ 
        printf("nnf_literal: VERSION FOURNIE A CORRIGER !\n");
        return var(x);
}

Nnf nnf_and(Nnf sonL, Nnf sonR) {
        /* A CHANGER */ 
        printf("nnf_and: VERSION FOURNIE A CORRIGER !\n");
        return and(sonL, sonR);
}

Nnf nnf_or(Nnf sonL, Nnf sonR) {
        /* A CHANGER */ 
        printf("nnf_or: VERSION FOURNIE A CORRIGER !\n");
        return or(sonL, sonR);
}

/* Translation */

Prop to_Prop(Nnf p) {
        return p;
}
